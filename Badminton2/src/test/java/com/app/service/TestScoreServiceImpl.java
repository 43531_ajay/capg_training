package com.app.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.app.entity.MatchEvent;
import com.app.entity.Players;
import com.app.entity.ScoreCard;
import com.app.repository.PlayerRepository;
import com.app.repository.ScoreRepository;


@RunWith(MockitoJUnitRunner.class)
public class TestScoreServiceImpl {
	@InjectMocks
	ScoreServiceImpl scoreService;
	
	
	@Mock
	ScoreRepository scoreRepository;
	
	@Mock
	PlayerRepository playerRepository;

	@Test
	public void fetchtheWinner() {
//		int lastMatchId = scoreRepository.getLastMatchId();
		List<ScoreCard> scoreDetails = new ArrayList<ScoreCard>();
		scoreDetails.add( new ScoreCard(1, "21/02/2016",1,2,1,2,1));
		scoreDetails.add( new ScoreCard(2, "21/02/2016",1,2,1,2,1));
//		/ScoreCard sc = scoreRepository.findByCurrentMatchMatchid(lastMatchId);
//		assertEquals(2,scoreDetails.size());
		//when(scoreRepository.getLastMatchId()).thenReturn(2);	
	//	assertEquals(2,scoreRepository.getLastMatchId() );
		ScoreCard test = new ScoreCard(2, "21/02/2016",1,2,1,2,1);
		ScoreCard test2 = scoreRepository.findByCurrentMatchMatchid(scoreRepository.getLastMatchId());
			
		when(scoreRepository.findByCurrentMatchMatchid(2).thenReturn(Optional.of(test2)));
		verify(test2,times(1)).findByCurrentMatchMatchid(2);
		
		
//		Players winner = playerRepository.findById(sc.getWinner()).get(); // we have to use .get() to avoid writing Optional<Player> in the LHS
		

	}
}
