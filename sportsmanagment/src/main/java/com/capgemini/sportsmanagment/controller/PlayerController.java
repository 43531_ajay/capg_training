package com.capgemini.sportsmanagment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties.Admin;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.sportsmanagment.entity.Players;
import com.capgemini.sportsmanagment.service.PlayerService;

@RestController
@RequestMapping("/player")
public class PlayerController {

	@Autowired
	private PlayerService playersService;
//
//	@RequestMapping("/home")
//	public String home() {
//		return "home";
//	}

//	@RequestMapping("/register")
//	public String playersRegistration() {
//      
//		return "register";
//	}
//	
	
	@PostMapping(value = "/savePlayer")
	public String playerRegistered(@RequestBody Players players) {
		playersService.savePlayer(players);
		return "registrationSuccessful";
	}
//    
	
//	@RequestMapping("/admin")
//	public String showForm(Model model) {
//		Admin user = new Admin();
//		model.addAttribute("user", user);
//		return "admin";
//	}

//	@RequestMapping(value = "/xcvbnm$zxcvbnm", method = RequestMethod.POST)
//	public String processForm(@Valid @ModelAttribute("user") Admin user, BindingResult bindingResult) {
//
//		if (bindingResult.hasErrors()) {
//			return "admin";
//		}
//		return "dashboard2";
////	}
//
//	@RequestMapping("/gameinfo")
//	public String gameInfo() {
//
//		return "gameinfo";
//	}

//	@RequestMapping("/registerSuccess")
//	public String playersRegistrationSuccess() {
//
//		return "successful";
//	}

	@RequestMapping("/getallplayers")
	public List<Players> getAllPlayers() {
		List<Players> players = playersService.getAllplayers();
		
		return players;

	}

	@RequestMapping("/getMalePlayers")
	public List<Players> getMPlayers() {
		List<Players> malePlayers = playersService.getMalePlayers();
	
		return malePlayers;

	}

	@RequestMapping("/getFemalePlayers")
	public String getFPlayers() {
		List<Players> malePlayers = playersService.getFemalePlayers();
		
		return getFPlayers();
	}
//	@RequestMapping("/deleteform")
//	public String deleteTraineeForm() {
//		return "deleteform";
//	}
//	
	
	
	@GetMapping("/getinfo")
	public Players deletePlayer(@RequestParam("playerId") Integer playerId) {
		System.out.println(playerId);

		Players player = playersService.fetchPlayerById(playerId);
		return player;
	}
	
	

	@RequestMapping("/delete/{playerId}")
	public String removeTrainee(@PathVariable("playerId") int playerId) {

		playersService.deletePlayers(playerId);

		return "Player deleted successfully!";

	}
}
