package com.capgemini.sportsmanagment.service;


import java.util.List;

import com.capgemini.sportsmanagment.entity.Players;
import com.capgemini.sportsmanagment.entity.ScoreCard;


public interface ScoreService {
	;
	public List<ScoreCard> fetchAllMatchRecord();

	public ScoreCard saveMatchRecord(int mid, ScoreCard matchRecord);
	public Players fetchtheWinner();
	Players fetchtheRunnerUp();
}
