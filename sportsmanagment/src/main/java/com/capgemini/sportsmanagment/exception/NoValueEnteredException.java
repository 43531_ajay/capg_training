package com.capgemini.sportsmanagment.exception;

import com.capgemini.sportsmanagment.entity.Players;

public class NoValueEnteredException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	private String message;

	public NoValueEnteredException(String message) {
		super(message);
	}

	@Override
	public String getMessage() {
		return super.getMessage();
	}

}
