package com.capgemini.sportsmanagment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.capgemini.sportsmanagment.entity.Players;
@Repository
public interface PlayerRepository extends JpaRepository<Players,Integer>{

	

//	@Query("select u from Player u where u.gender=:rl")
//	public List<Player> findByGender(@Param("rl") Player gender);

	
	List<Players> findByGender(String gender);
	
	
}
