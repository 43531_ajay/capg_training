package com.capgemini.sportsmanagment.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.capgemini.sportsmanagment.entity.MatchEvent;

public interface MatchEventRepository extends JpaRepository<MatchEvent,Integer>{

}
