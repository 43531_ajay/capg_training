package com.capgemini.sportsmanagment.service;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.capgemini.sportsmanagment.entity.Players;
import com.capgemini.sportsmanagment.exception.DuplicatePlayerException;
import com.capgemini.sportsmanagment.exception.PlayerNotFoundException;
import com.capgemini.sportsmanagment.repository.PlayerRepository;

@Service
public class PlayerServiceImpl implements PlayerService {
	
	@Autowired
	private PlayerRepository playerRepository;

	@Override
	public Players savePlayer(Players player) throws DuplicatePlayerException  {
		
		Optional<Players> playerObj = playerRepository.findById(player.getPlayerId());
		
        if (playerObj.isPresent()) {
            throw new DuplicatePlayerException("Player Already Existing With Id: "+player.getPlayerId());
        }       
		
		return playerRepository.save(player);
			
	}

	@Override
	public void deletePlayer(int playerId) throws PlayerNotFoundException {
		
		Optional<Players> players = playerRepository.findById(playerId);
		if(players.isEmpty()) {
			throw new PlayerNotFoundException("No Player is Existing With id: "+playerId);
			
		}
		
		playerRepository.deleteById(playerId);
		
	}



	@Override
	public Players fetchPlayerById(int playerId) throws PlayerNotFoundException {	
		
		Optional<Players> player = playerRepository.findById(playerId);
		if(player.isEmpty()) {
			throw new PlayerNotFoundException("No Player is Existing With id: "+playerId);
			
		}
		return playerRepository.findById(playerId).get();		
	}

	@Override
	public List<Players> fetchAllPlayer() {
		
		return playerRepository.findAll();
	}

	@Override
	public List<Players> getMalePlayers() {
		List<Players> list = playerRepository.findByGender("male");
		for (Players p: list) {
			System.out.println(p);
		}
		return list;

	}

	@Override
	public List<Players> getFemalePlayers() {
		List<Players> list = playerRepository.findByGender("female");
		for (Players p: list) {
			System.out.println(p);
		}
		return list;
	}
}