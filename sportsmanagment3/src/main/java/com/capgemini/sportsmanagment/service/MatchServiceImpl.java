package com.capgemini.sportsmanagment.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.sportsmanagment.entity.MatchEvent;
import com.capgemini.sportsmanagment.entity.ScoreCard;
//import com.capgemini.sportsmanagment.exception.NoValueEnteredException;
import com.capgemini.sportsmanagment.repository.MatchEventRepository;
import com.capgemini.sportsmanagment.repository.ScoreRepository;

@Service
public class MatchServiceImpl implements MatchService {

	@Autowired
	private MatchEventRepository matchRepository;

	@Autowired
	private ScoreRepository scoreRepository;

	@Override
	public List<MatchEvent> getAllFixtures() {

		return matchRepository.findAll();
	}

	@Override
	public MatchEvent saveMatchEvent(MatchEvent event) {

//		Optional<MatchEvent> eventObj = matchRepository.findById(event.getMatchid());

		ScoreCard sc = new ScoreCard();
		sc.setCurrentMatch(event);
		sc.setDate(event.getDate());
		sc.setPlayerid1(event.getPlayerid1());
		sc.setPlayerid2(event.getPlayerid2());

		scoreRepository.save(sc);
		event.setMatchScoreCard(sc);
		return matchRepository.save(event);

	}

}
