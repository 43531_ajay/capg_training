package com.capgemini.sportsmanagment.service;


import java.util.List;
import com.capgemini.sportsmanagment.entity.Players;
import com.capgemini.sportsmanagment.exception.DuplicatePlayerException;
import com.capgemini.sportsmanagment.exception.PlayerNotFoundException;



public interface PlayerService {	

	public Players savePlayer(Players players) throws DuplicatePlayerException;
	
	public void deletePlayer(int playerId) throws PlayerNotFoundException ;
	
	public Players fetchPlayerById(int playerId) throws PlayerNotFoundException;
	
	public List<Players> getMalePlayers();
	
	public List<Players> getFemalePlayers();
	
	public List<Players> fetchAllPlayer();
	
}


