package com.capgemini.sportsmanagment.controller;


import java.util.ArrayList;
import java.util.List;
//import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.capgemini.sportsmanagment.entity.Players;
import com.capgemini.sportsmanagment.entity.ScoreCard;
//import com.capgemini.sportsmanagment.exception.DuplicatePlayerException;
import com.capgemini.sportsmanagment.service.ScoreService;

@Controller
@RequestMapping("/admin")
public class ScoreCardController {

	@Autowired
	private ScoreService scoreService;


//
//	@RequestMapping("/matchPlay")
//	public String matchPlayground() {
//		return "matchplay";
//	}

	@RequestMapping(value = "/saveMatchRecord", method = RequestMethod.POST)
	public ResponseEntity<String> matchRecordSave(@RequestParam("mid") int mid, @RequestParam("winner") int winner,
			@RequestBody ScoreCard matchRecord) {
		
		System.out.println("match ID from JSP: " + mid);
		matchRecord.setWinner(winner);
		System.out.println(matchRecord);
		
		scoreService.saveMatchRecord(mid, matchRecord);
		System.out.println("before return stmt");
		return new ResponseEntity<>("saved match record!", HttpStatus.OK);
	}

	@RequestMapping("/getAllMatchRecords")
	public ResponseEntity<List<ScoreCard>> showAllMatchRecords() {
	//	List<ScoreCard> matchRecords = scoreService.fetchAllMatchRecord();
		return new ResponseEntity<>(scoreService.fetchAllMatchRecord(), HttpStatus.OK);
	}

	@RequestMapping("/getTheWinners") // and the runnerup also
	public ResponseEntity<List> fetchtheWinner() {

		 

        Players winner = scoreService.fetchtheWinner();
        Players runnerup = scoreService.fetchtheRunnerUp();
        List<Players> list=new ArrayList<Players>();
        list.add(winner);
        list.add(runnerup);
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

}