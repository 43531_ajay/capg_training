package com.capgemini.sportsmanagment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.sportsmanagment.entity.MatchEvent;
import com.capgemini.sportsmanagment.exception.DuplicatePlayerException;
import com.capgemini.sportsmanagment.exception.NoValueEnteredException;
import com.capgemini.sportsmanagment.service.MatchService;





@RestController
@RequestMapping("/admin")
public class AdminController{
	@Autowired
	private MatchService matchService;
	
	
//	@GetMapping("/dashboard2")
//	public String showDashboard() {
//		return "dashboard2";
//	}
//	@GetMapping("/addevent")
//	public String addEventForm(Model model) {
//		MatchEvent event = new MatchEvent();	
//		model.addAttribute("event", event);
//		return "addevent";
//	}
//	
	@RequestMapping(value="/save",method = RequestMethod.POST)
	public String saveMatchEvent(@RequestBody MatchEvent event) {
		System.out.println("Evenet: "+event);
		if (event.getPlayerid1()==0 || event.getPlayerid2()==0 || event.getDate()==null)
			throw new NoValueEnteredException("Please enter Values!");
		if(event.getPlayerid1()==event.getPlayerid2())
			throw new DuplicatePlayerException("Duplicate player entered!");
		matchService.saveMatchEvent(event);	
        return "added new match event";
	
	}	
	
	@RequestMapping("/getAllFixtures")
	public List<MatchEvent> getAllMatches() {
		List<MatchEvent> events = matchService.getAllFixtures();
	
		events.forEach(System.out::println);
		return events;

	}
	}



