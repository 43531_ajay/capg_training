package com.capgemini.sportsmanagment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.capgemini.sportsmanagment.entity.Players;
import com.capgemini.sportsmanagment.service.PlayerService;

@RestController
@RequestMapping("/players")
public class PlayerController 
{
	@Autowired
	private PlayerService playerService;
	
	@GetMapping("/getall")
	public List<Players> getAllPlayers() 
	{
		
		List<Players> players = playerService.fetchAllPlayer();
		
		return players;
	}
	
	@GetMapping("/getall/{playerId}")
	public Players getPlayer(@PathVariable("playerId") int PlayerId) {
		
		Players player = playerService.fetchPlayerById(PlayerId);	
		return player;
	}
	
	@GetMapping("/getMalePlayers")
	public List<Players> getMPlayers() {
		List<Players> malePlayers = playerService.getMalePlayers();
	
		return malePlayers;

	}
	
	@GetMapping("/getFemalePlayers")
	public List<Players> getFPlayers() {
		List<Players> femalePlayers = playerService.getFemalePlayers();
		
		return femalePlayers;
	}
	
	@PostMapping("/save")
	public String savePlayer(@RequestBody Players players) {
		
		playerService.savePlayer(players);
		
		return "Registration Successful";
	}
	
	@DeleteMapping("/{playerId}")
	public String deleteTrainee(@PathVariable("playerId") int playerId) {
		playerService.deletePlayer(playerId);
		
		return "Player Deleted ";
	}
	
	
}
