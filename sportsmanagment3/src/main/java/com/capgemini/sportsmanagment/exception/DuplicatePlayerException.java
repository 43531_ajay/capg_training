package com.capgemini.sportsmanagment.exception;

import com.capgemini.sportsmanagment.entity.Players;

public class DuplicatePlayerException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	private Players player;
    
    private String message;
    
    public DuplicatePlayerException(String message) {
        super(message);
    }

    public DuplicatePlayerException(Players player) {
        this.player = player;
    }

	public Players getPlayer() {
		return player;
	}	
	
}